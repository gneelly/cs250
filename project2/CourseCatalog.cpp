#include "CourseCatalog.hpp"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <stack>
using namespace std;

#include "UTILITIES/Menu.hpp"
#include "EXCEPTIONS/CourseNotFoundException.hpp"
#include "DATA_STRUCTURES/Stack.hpp"

CourseCatalog::CourseCatalog()
{
    LoadCourses();
}

void CourseCatalog::LoadCourses() // done
{
    Menu::Header( "LOADING COURSES" );

    ifstream input( "courses.txt" );

    if ( !input.is_open() )
    {
        cout << "Error opening input text file, courses.txt" << endl;
        return;
    }

    string label, courseCode, courseName, prerequisite;
    Course newCourse;

    while ( input >> label )
    {
        if ( label == "COURSE" )
        {
            if ( newCourse.name != "" )
            {
                m_courses.PushBack( newCourse );
                newCourse.Clear();
            }

            input >> newCourse.code >> newCourse.name;
        }
        else if ( label == "PREREQ" )
        {
            input >> newCourse.prereq;
        }
    }

    input.close();

    cout << " * " << m_courses.Size() << " courses loaded" << endl << endl;
}

void CourseCatalog::ViewCourses()
{
    Menu::Header( "VIEW COURSES" );
	cout << "#    CODE       TITLE" << endl << "       PREREQ" << endl;
	Menu::DrawHorizontalBar(80);
	for (int i = 0; i < m_courses.Size(); i++) {
		cout << i << "    " << m_courses[i].code << "    " << m_courses[i].name << endl;
		cout << "       " << m_courses[i].prereq << endl;
	}
}

Course CourseCatalog::FindCourse( const string& code )
{

	for (int i = 0; i < m_courses.Size(); i++) {
		if (code == m_courses[i].code) {
			return m_courses[i];
		}
	}
	throw CourseNotFound("Course Not Found");
}

void CourseCatalog::ViewPrereqs()
{
    Menu::Header( "GET PREREQS" );
	string code;
	Course current;
	int count = 1;
	cout << "Enter class code" << endl << endl << " >> ";
	getline(cin, code);
	cout << endl;
	try {
		current = FindCourse(code);
	}
	catch(CourseNotFound& ex){
		cout << "Error! Unable to find course " << code << "!" << endl;
		return;
	}
	LinkedStack<Course> prereqs;

	cout << "Classes to take : " << endl;
	prereqs.Push(current);
	while (current.prereq != "") {
		try {
			current = FindCourse(current.prereq);
			prereqs.Push(current);
		}
		catch (CourseNotFound& ex) {
			break;
		}
	}
	while(prereqs.Size() > 0){
		cout << count <<".  "<< prereqs.Top().code << " " << prereqs.Top().name << endl;
		prereqs.Pop();
		count++;
	}
}

void CourseCatalog::Run()
{
    bool done = false;
    while ( !done )
    {
        Menu::Header( "MAIN MENU" );

        int choice = Menu::ShowIntMenuWithPrompt( { "View all courses", "Get course prerequisites", "Exit" } );

        switch( choice )
        {
            case 1:
                ViewCourses();
            break;

            case 2:
                ViewPrereqs();
            break;

            case 3:
                done = true;
            break;
        }
    }
}
