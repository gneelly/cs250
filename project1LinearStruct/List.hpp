#ifndef _LIST_HPP
#define _LIST_HPP

#include <iostream>
using namespace std;

const int ARRAY_SIZE = 100;

template <typename T>
class List
{
private:
	// private member variables
	int m_itemCount;
	T m_arr[ARRAY_SIZE];

	// functions for interal-workings
	bool ShiftRight(int atIndex)
	{
		if (atIndex < 0 || atIndex > m_itemCount)
		{
			return false;
		}
		m_itemCount++;
		for (int i = m_itemCount; i > atIndex; i--) {
			m_arr[i] = m_arr[i - 1];
		}
		return true;

	}

	bool ShiftLeft(int atIndex)
	{
		if (atIndex < 0 || atIndex > m_itemCount)
		{
			return false;
		}

		for (int i = atIndex + 1; i < m_itemCount; i++)
		{
			m_arr[i - 1] = m_arr[i];
		}

		return true;
	}

public:
	List()
	{
		m_itemCount = 0;
	}

	~List()
	{
	}

	// Core functionality
	int     Size() const
	{
		return m_itemCount;
	}

	bool    IsEmpty() const
	{
		return (m_itemCount == 0);
	}

	bool    IsFull() const
	{
		return (m_itemCount == ARRAY_SIZE);
	}

	bool    PushFront(const T& newItem)
	{

		if (IsFull()) { return false; }
		if (IsEmpty())
		{
			m_arr[0] = newItem;
			m_itemCount++;
			return true;
		}
		else {

			for (int i = m_itemCount; i > 0; i--) {
				m_arr[i] = m_arr[i - 1];
			}
			m_arr[0] = newItem;
			m_itemCount++;
			return true;
		}

		return false;
	}

	bool    PushBack(const T& newItem)
	{

		if (IsFull()) { return false; }

		m_arr[m_itemCount] = newItem;
		m_itemCount++;
		return true;
	}

	bool    Insert(int atIndex, const T& item)
	{
		
		if (atIndex < 0 || atIndex > m_itemCount)
		{
			return false;
		}
		ShiftRight(atIndex);
		m_arr[atIndex] = item;
		return true;
	}


	bool    PopFront()
	{

		if (IsEmpty())
		{
			return false;
		}

		for (int i = 0; i < m_itemCount; i++) {
			m_arr[i] = m_arr[i + 1];
		}
		m_itemCount--;
		return true;
	}

	bool    PopBack()
	{
		if (IsEmpty()) { return false; }

		// lazy deletion
		m_itemCount--;
		return true;
	}

	bool    RemoveItem(const T& item)
	{
		if (IsEmpty())
		{
			return false;
		}

		// One way you can do this
		int removeAtIndex[ARRAY_SIZE];
		int j = 0;

		// Keep track of indices to remove
		for (int i = 0; i < Size(); i++)
		{
			if (m_arr[i] == item)
			{
				removeAtIndex[j] = i;
				j++;
			}
		}

		// Remove all indices we recorded
		for (int i = 0; i < j; i++)
		{
			RemoveIndex(removeAtIndex[i]);
		}
	}

	bool    RemoveIndex(int atIndex)
	{
		if (IsEmpty())
		{
			return false;
		}
		if (atIndex < 0 || atIndex > m_itemCount)
		{
			return false;
		}
			ShiftLeft(atIndex);
			m_itemCount--;
			return true;
	}

	void    Clear()
	{
		// lazy deletion
		m_itemCount = 0;
	}

	// Accessors
	T*      Get(int atIndex)
	{
		if (atIndex < 0 || atIndex >= m_itemCount)
		{
			return nullptr;
		}

		return &m_arr[atIndex];
	}

	T*      GetFront()
	{
		if (IsEmpty()) {
			return false;
		}
		return &m_arr[0];
	}

	T*      GetBack()
	{
		if (IsEmpty()) {
			return false;
		}
		return &m_arr[m_itemCount - 1];
	}

	// Additional functionality
	int     GetCountOf(const T& item) const
	{
		int counter = 0;
		for (int i = 0; i < ARRAY_SIZE; i++) {
			if (m_arr[i] == item) {
				counter++;
			}
		}
		return counter;
	}

	bool    Contains(const T& item) const
	{
		if (GetCountOf(item) > 0) {
			return true;
		}
		else {
			return false;
		}
	}

	// Helper function
	void Display()
	{
		cout << "\t List size: " << Size() << endl;
		for (int i = 0; i < Size(); i++)
		{
			T* item = Get(i);

			cout << "\t " << i << " = ";

			if (item == nullptr)
			{
				cout << "nullptr" << endl;
			}
			else
			{
				cout << *item << endl;
			}
		}
	}

	friend class Tester;
};


#endif
