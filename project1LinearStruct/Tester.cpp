#include "Tester.hpp"

/*
This is the testers we are working on for project 1,
but there have been modifications for the following:

* Functions that used to return true on success and false on fail
    have been updated to use exceptions, so they have no return type.
    Therefore, these tests aren't needed and were removed.

* With a linked list there is no "max value" so tests for when the
    list was full was removed.
*/

void Tester::RunTests()
{
	Test_Init();
    Test_IsEmpty();
    Test_IsFull();
    Test_Size();
    Test_GetCountOf();
    Test_Contains();

    Test_PushFront();
    Test_PushBack();

    Test_Get();
    Test_GetFront();
    Test_GetBack();

    Test_PopFront();
    Test_PopBack();
    Test_Clear();

    Test_ShiftRight();
    Test_ShiftLeft();

    Test_Remove();
    Test_Insert();
}

void Tester::DrawLine()
{
    cout << endl;
    for ( int i = 0; i < 80; i++ )
    {
        cout << "-";
    }
    cout << endl;
}

void Tester::Test_Init()
{
    DrawLine();
    cout << "TEST: Test_Init" << endl;

	List<string> testlist;
	cout << endl << "Test: Make sure that m_itemCount = 0." << endl;
	int expectedSize = 0;
	int actualSize = testlist.Size();

	cout << "Expected size: " << expectedSize << endl;
	cout << "Actual size:   " << actualSize << endl;

	if (actualSize == expectedSize)
	{
		cout << "Pass" << endl;
	}
	else
	{
		cout << "Fail" << endl;
	}
}

void Tester::Test_ShiftRight()
{
    DrawLine();
    cout << "TEST: Test_ShiftRight" << endl;

	{
		cout << endl << "Test 1: Push Front 'A', 'B', 'C', ShiftRight(1) should return Get(0) = A, Get(2) = B, Get(3) = C" << endl;

		List<string> testlist;
		testlist.PushBack("A");
		testlist.PushBack("B");
		testlist.PushBack("C");
		testlist.ShiftRight(1);
		string expectedValue = "A";
		string* actualValue = testlist.Get(0);
		string expectedValue2 = "B";
		string* actualValue2 = testlist.Get(2);
		string expectedValue3 = "C";
		string* actualValue3 = testlist.Get(3);

		if (actualValue == nullptr)
		{
			cout << "Nullptr" << endl;
			return;
		}

		cout << "Value, expected: " << expectedValue << ", actual: " << *actualValue << endl;
		cout << "Value, expected: " << expectedValue2 << ", actual: " << *actualValue2 << endl;
		cout << "Value, expected: " << expectedValue3 << ", actual: " << *actualValue3 << endl;


		if (*actualValue != expectedValue||
			*actualValue2 != expectedValue2||
			* actualValue3 != expectedValue3)
		{
			cout << "FAIL - Bad value" << endl;
		}
		else
		{
			cout << "PASS" << endl;
		}
	}
	{
		cout << endl << "Test 1: Push Front 'A', 'B', 'C' 'D', ShiftRight(1) should return Get(3) = C" << endl;

		List<string> testlist;
		testlist.PushBack("A");
		testlist.PushBack("B");
		testlist.PushBack("C");
		testlist.PushBack("D");

		testlist.ShiftRight(1);
		string expectedValue = "C";
		string* actualValue = testlist.Get(3);

		if (actualValue == nullptr)
		{
			cout << "Nullptr" << endl;
			return;
		}

		cout << "Value, expected: " << expectedValue << ", actual: " << *actualValue << endl;

		if (*actualValue != expectedValue)
		{
			cout << "FAIL - Bad value" << endl;
		}
		else
		{
			cout << "PASS" << endl;
		}
		
	}
}

void Tester::Test_ShiftLeft()
{
    DrawLine();
    cout << "TEST: Test_ShiftLeft" << endl;

	{
		cout << endl << "Test 1: Push Front 'A', 'B', 'C', ShiftLeft(1) should return Get(1) = C" << endl;

		List<string> testlist;
		testlist.PushBack("A");
		testlist.PushBack("B");
		testlist.PushBack("C");
		testlist.ShiftLeft(1);
		string expectedValue = "A";
		string* actualValue = testlist.Get(0);
		string expectedValue2 = "C";
		string* actualValue2 = testlist.Get(1);

		if (actualValue == nullptr)
		{
			cout << "Nullptr" << endl;
			return;
		}

		cout << "Value, expected: " << expectedValue << ", actual: " << *actualValue << endl;
		cout << "Value, expected: " << expectedValue2 << ", actual: " << *actualValue2 << endl;


		if (*actualValue != expectedValue ||
			*actualValue2 != expectedValue2)
		{
			cout << "FAIL - Bad value" << endl;
		}
		else
		{
			cout << "PASS" << endl;
		}
	
	}
	{
		cout << endl << "Test 2: Push Front 'A', 'B', 'C' 'D', ShiftLeft(1) should return Get(3) = C" << endl;

		List<string> testlist;
		testlist.PushBack("A");
		testlist.PushBack("B");
		testlist.PushBack("C");
		testlist.PushBack("D");
		testlist.ShiftLeft(1);
		string expectedValue = "D";
		string* actualValue = testlist.Get(3);

		if (actualValue == nullptr)
		{
			cout << "Nullptr" << endl;
			return;
		}

		cout << "Value, expected: " << expectedValue << ", actual: " << *actualValue << endl;

		if (*actualValue != expectedValue)
		{
			cout << "FAIL - Bad value" << endl;
		}
		else
		{
			cout << "PASS" << endl;
		}
		
	}
}

void Tester::Test_Size()
{
    DrawLine();
    cout << "TEST: Test_Size" << endl;

    {   // Test begin
        cout << endl << "Test 1" << endl;
        List<int> testList;
        int expectedSize = 0;
        int actualSize = testList.Size();

        cout << "Expected size: " << expectedSize << endl;
        cout << "Actual size:   " << actualSize << endl;

        if ( actualSize == expectedSize )
        {
            cout << "Pass" << endl;
        }
        else
        {
            cout << "Fail" << endl;
        }
    }   // Test end

    {   // Test begin
        cout << endl << "Test 2" << endl;
        List<int> testList;

		for (int i = 0; i < 5; i++)
		{
			testList.PushFront(1);
		}

        int expectedSize = 5;
        int actualSize = testList.Size();

        cout << "Expected size: " << expectedSize << endl;
        cout << "Actual size:   " << actualSize << endl;

        if ( actualSize == expectedSize )
        {
            cout << "Pass" << endl;
        }
        else
        {
            cout << "Fail" << endl;
        }
    }   // Test end

	{   // Test begin
		cout << endl << "Test 2" << endl;
		List<int> testList;

		for (int i = 0; i < 100; i++)
		{
			testList.PushFront(1);
		}

		int expectedSize = 100;
		int actualSize = testList.Size();

		cout << "Expected size: " << expectedSize << endl;
		cout << "Actual size:   " << actualSize << endl;

		if (actualSize == expectedSize)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end
}

void Tester::Test_IsEmpty()
{
    DrawLine();
    cout << "TEST: Test_IsEmpty" << endl;

    // Put tests here
    {
        // Test 1
        cout << endl << "Test 1" << endl;
        List<int> listA;
        bool expectedValue = true;
        bool actualValue = listA.IsEmpty();

        cout << "Created list, didn't add anything, should be empty..." << endl;
        cout << "Expected value: " << expectedValue << endl;
        cout << "Actual value:   " << actualValue << endl;

        if ( expectedValue == actualValue )
        {
            cout << "Test passed" << endl;
        }
        else
        {
            cout << "Test failed" << endl;
        }
    }

    {
        // Test 2
        cout << endl << "Test 1" << endl;
        List<int> listA;
        listA.PushBack( 5 );
        bool expectedValue = false;
        bool actualValue = listA.IsEmpty();

        cout << "Created list, added one thing, shouldn't be empty..." << endl;
        cout << "Expected value: " << expectedValue << endl;
        cout << "Actual value:   " << actualValue << endl;

        if ( expectedValue == actualValue )
        {
            cout << "Test passed" << endl;
        }
        else
        {
            cout << "Test failed" << endl;
        }
    }
}

void Tester::Test_IsFull()
{
    DrawLine();
    cout << "TEST: Test_IsFull" << endl;
	{
		List<string> testlist;
	
		bool expectedReturn = false;
		bool actualReturn = testlist.IsFull();

		int expectedValue = 0;
		int actualValue = testlist.Size();

		cout << "Return, expected: " << expectedReturn << ", actual: " << actualReturn << endl;
		cout << "Size, expected: " << expectedValue << ", actual: " << actualValue << endl;

		if (expectedValue != actualValue)
		{
			cout << "FAIL - Bad size" << endl;
		}
		else if (expectedReturn != actualReturn)
		{
			cout << "FAIL - Bad return" << endl;
		}
		else
		{
			cout << "PASS" << endl;
		}
	}
	{
		List<string> testlist;
		for (int i = 0; i < 5; i++)
		{
			testlist.PushFront("Z");
		}

		bool expectedReturn = false;
		bool actualReturn = testlist.IsFull();

		int expectedValue = 5;
		int actualValue = testlist.Size();

		cout << "Return, expected: " << expectedReturn << ", actual: " << actualReturn << endl;
		cout << "Size, expected: " << expectedValue << ", actual: " << actualValue << endl;

		if (expectedValue != actualValue)
		{
			cout << "FAIL - Bad size" << endl;
		}
		else if (expectedReturn != actualReturn)
		{
			cout << "FAIL - Bad return" << endl;
		}
		else
		{
			cout << "PASS" << endl;
		}
	}
	{
		List<string> testlist;
		for (int i = 0; i < 100; i++)
		{
			testlist.PushFront("Z");
		}

		bool expectedReturn = true;
		bool actualReturn = testlist.IsFull();

		int expectedValue = 100;
		int actualValue = testlist.Size();

		cout << "Return, expected: " << expectedReturn << ", actual: " << actualReturn << endl;
		cout << "Size, expected: " << expectedValue << ", actual: " << actualValue << endl;

		if (expectedValue != actualValue)
		{
			cout << "FAIL - Bad size" << endl;
		}
		else if (expectedReturn != actualReturn)
		{
			cout << "FAIL - Bad return" << endl;
		}
		else
		{
			cout << "PASS" << endl;
		}
	}
	{
		List<string> testlist;
		for (int i = 0; i < 101; i++)
		{
			testlist.PushFront("Z");
		}

		bool expectedReturn = true;
		bool actualReturn = testlist.IsFull();

		int expectedValue = 100;
		int actualValue = testlist.Size();

		cout << "Return, expected: " << expectedReturn << ", actual: " << actualReturn << endl;
		cout << "Size, expected: " << expectedValue << ", actual: " << actualValue << endl;

		if (expectedValue != actualValue)
		{
			cout << "FAIL - Bad size" << endl;
		}
		else if (expectedReturn != actualReturn)
		{
			cout << "FAIL - Bad return" << endl;
		}
		else
		{
			cout << "PASS" << endl;
		}
	}
	
}

void Tester::Test_PushFront()
{
    DrawLine();
    cout << "TEST: Test_PushFront" << endl;

	{
		cout << endl << "Test 1: Create a list, input 'A'." << endl;

		List<string> testlist;

		// Things to test

		bool expectedReturn = true;
		bool actualReturn = testlist.PushFront("A");

		int expectedSize = 1;
		int actualSize = testlist.Size();

		// Remember that Get functions return a pointer in this project.
		string expectedValueAt0 = "A";
		string* actualValueAt0 = testlist.Get(0);

		if (actualValueAt0 == nullptr)
		{
			cout << "nullptr - exiting test" << endl;
			return;
		}

		// Display checks
		cout << "Return, expected: " << expectedReturn << ", actual: " << actualReturn << endl;
		cout << "Size, expected: " << expectedSize << ", actual: " << actualSize << endl;
		cout << "Value, expected: " << expectedValueAt0 << ", actual: " << *actualValueAt0 << endl;

		// Check each value
		if (expectedReturn != actualReturn)
		{
			cout << "FAIL - Bad return" << endl;
		}
		else if (expectedSize != actualSize)
		{
			cout << "FAIL - Bad size" << endl;
		}
		else if (expectedValueAt0 != *actualValueAt0)
		{
			cout << "FAIL - Bad value" << endl;
		}
		else
		{
			cout << "PASS" << endl;
		}
	}

	{
		cout << endl << "Test 2: Push Front 'A', 'B', 'C', make sure 0 = A, 1 = B, 2 = C" << endl;

		List<string> testlist;
		testlist.PushFront("A");
		testlist.PushFront("B");
		testlist.PushFront("C");

		string expectedValue_0 = "C";
		string expectedValue_1 = "B";
		string expectedValue_2 = "A";

		string* actualValue_0 = testlist.Get(0);
		string* actualValue_1 = testlist.Get(1);
		string* actualValue_2 = testlist.Get(2);

		if (actualValue_0 == nullptr || actualValue_1 == nullptr || actualValue_2 == nullptr)
		{
			cout << "nullptrs returned; exiting to avoid segfault" << endl;
			return;
		}

		cout << "Expected value at 0: " << expectedValue_0 << endl;
		cout << "Expected value at 1: " << expectedValue_1 << endl;
		cout << "Expected value at 2: " << expectedValue_2 << endl;

		cout << "Actual value at 0: " << *actualValue_0 << endl;
		cout << "Actual value at 1: " << *actualValue_1 << endl;
		cout << "Actual value at 2: " << *actualValue_2 << endl;

		if (expectedValue_0 != *actualValue_0 ||
			expectedValue_1 != *actualValue_1 ||
			expectedValue_2 != *actualValue_2)
		{
			cout << "FAIL - Bad value(s)" << endl;
		}
		else
		{
			cout << "PASS" << endl;
		}
	}

	{
		cout << endl << "Test 3: Create a list, insert 101 items, Size() should return 100" << endl;

		List<string> testlist;
		for (int i = 0; i < 100; i++)
		{
			testlist.PushFront("Z");
		}

		bool expectedReturn = false;
		bool actualReturn = testlist.PushFront("A");

		int expectedValue = 100;
		int actualValue = testlist.Size();

		cout << "Return, expected: " << expectedReturn << ", actual: " << actualReturn << endl;
		cout << "Size, expected: " << expectedValue << ", actual: " << actualValue << endl;

		if (expectedValue != actualValue)
		{
			cout << "FAIL - Bad size" << endl;
		}
		else if (expectedReturn != actualReturn)
		{
			cout << "FAIL - Bad return" << endl;
		}
		else
		{
			cout << "PASS" << endl;
		}
	}
}

void Tester::Test_PushBack()
{
    DrawLine();
    cout << "TEST: Test_PushBack" << endl;
    {
        cout << endl << "Test 1: Create a list, input 'A'." << endl;

        List<string> testlist;

        // Things to test

        bool expectedReturn = true;
        bool actualReturn = testlist.PushBack( "A" );

        int expectedSize = 1;
        int actualSize = testlist.Size();

        // Remember that Get functions return a pointer in this project.
        string expectedValueAt0 = "A";
        string* actualValueAt0 = testlist.Get(0);

        if ( actualValueAt0 == nullptr )
        {
            cout << "nullptr - exiting test" << endl;
            return;
        }

        // Display checks
        cout << "Return, expected: " << expectedReturn << ", actual: " << actualReturn << endl;
        cout << "Size, expected: " << expectedSize << ", actual: " << actualSize << endl;
        cout << "Value, expected: " << expectedValueAt0 << ", actual: " << *actualValueAt0 << endl;

        // Check each value
        if ( expectedReturn != actualReturn )
        {
            cout << "FAIL - Bad return" << endl;
        }
        else if ( expectedSize != actualSize )
        {
            cout << "FAIL - Bad size" << endl;
        }
        else if ( expectedValueAt0 != *actualValueAt0 )
        {
            cout << "FAIL - Bad value" << endl;
        }
        else
        {
            cout << "PASS" << endl;
        }
    }

    {
        cout << endl << "Test 2: Push Back 'A', 'B', 'C', make sure 0 = A, 1 = B, 2 = C" << endl;

        List<string> testlist;
        testlist.PushBack( "A" );
        testlist.PushBack( "B" );
        testlist.PushBack( "C" );

        string expectedValue_0 = "A";
        string expectedValue_1 = "B";
        string expectedValue_2 = "C";

        string* actualValue_0 = testlist.Get( 0 );
        string* actualValue_1 = testlist.Get( 1 );
        string* actualValue_2 = testlist.Get( 2 );

        if ( actualValue_0 == nullptr || actualValue_1 == nullptr || actualValue_2 == nullptr )
        {
            cout << "nullptrs returned; exiting to avoid segfault" << endl;
            return;
        }

        cout << "Expected value at 0: " << expectedValue_0 << endl;
        cout << "Expected value at 1: " << expectedValue_1 << endl;
        cout << "Expected value at 2: " << expectedValue_2 << endl;

        cout << "Actual value at 0: " << *actualValue_0 << endl;
        cout << "Actual value at 1: " << *actualValue_1 << endl;
        cout << "Actual value at 2: " << *actualValue_2 << endl;

        if ( expectedValue_0 != *actualValue_0 ||
             expectedValue_1 != *actualValue_1 ||
             expectedValue_2 != *actualValue_2 )
        {
            cout << "FAIL - Bad value(s)" << endl;
        }
        else
        {
            cout << "PASS" << endl;
        }
    }

    {
        cout << endl << "Test 3: Create a list, insert 101 items, Size() should return 100" << endl;

        List<string> testlist;
        for ( int i = 0; i < 100; i++ )
        {
            testlist.PushBack( "Z" );
        }

        bool expectedReturn = false;
        bool actualReturn = testlist.PushBack( "A" );

        int expectedValue = 100;
        int actualValue = testlist.Size();

        cout << "Return, expected: " << expectedReturn << ", actual: " << actualReturn << endl;
        cout << "Size, expected: " << expectedValue << ", actual: " << actualValue << endl;

        if ( expectedValue != actualValue )
        {
            cout << "FAIL - Bad size" << endl;
        }
        else if ( expectedReturn != actualReturn )
        {
            cout << "FAIL - Bad return" << endl;
        }
        else
        {
            cout << "PASS" << endl;
        }
    }
}

void Tester::Test_PopFront()
{
    DrawLine();
    cout << "TEST: Test_PopFront" << endl;

    // Put tests here
    {
        cout << endl << "Test 1: Create an empty list. PopFront should return false." << endl;

        List<int> testlist;

        bool expectedReturn = false;
        bool actualReturn = testlist.PopFront();

        cout << "Return, expected: " << expectedReturn << ", actual: " << actualReturn << endl;

        if ( expectedReturn != actualReturn )
        {
            cout << "FAIL - Bad return" << endl;
        }
        else
        {
            cout << "PASS" << endl;
        }
    }

    {
        cout << endl << "Test 2: PopFront 'A', 'B', 'C', make sure PopFront removes correct item." << endl;

        List<string> testlist;

        testlist.PushBack( "A" );
        testlist.PushBack( "B" );
        testlist.PushBack( "C" );

        bool expectedReturn = true;
        bool actualReturn = testlist.PopFront();

        string expected0 = "B";
        string* actual0 = testlist.GetFront();

        if ( actual0 == nullptr )
        {
            cout << "nullptr" << endl;
            return;
        }

        cout << "Return, expected: " << expectedReturn << ", actual: " << actualReturn << endl;
        cout << "Item at 0, expected: " << expected0 << ", actual: " << *actual0 << endl;

        if ( expectedReturn != actualReturn )
        {
            cout << "FAIL - Bad return" << endl;
        }
        else if ( expected0 != *actual0 )
        {
            cout << "FAIL - Bad value" << endl;
        }
        else
        {
            cout << "PASS" << endl;
        }
    }
}

void Tester::Test_PopBack()
{
    DrawLine();
    cout << "TEST: Test_PopBack" << endl;

	// Put tests here
	{
		cout << endl << "Test 1: Create an empty list. Pop should return false." << endl;

		List<int> testlist;

		bool expectedReturn = false;
		bool actualReturn = testlist.PopBack();

		cout << "Return, expected: " << expectedReturn << ", actual: " << actualReturn << endl;

		if (expectedReturn != actualReturn)
		{
			cout << "FAIL - Bad return" << endl;
		}
		else
		{
			cout << "PASS" << endl;
		}
	}

	{
		cout << endl << "Test 2: PopBack 'A', 'B', 'C', make sure PopFront removes correct item." << endl;

		List<string> testlist;

		testlist.PushBack("A");
		testlist.PushBack("B");
		testlist.PushBack("C");

		bool expectedReturn = true;
		bool actualReturn = testlist.PopBack();

		string expectedEnd = "B";
		string* actualEnd = testlist.GetBack();

		if (actualEnd == nullptr)
		{
			cout << "nullptr" << endl;
			return;
		}

		cout << "Return, expected: " << expectedReturn << ", actual: " << actualReturn << endl;
		cout << "Item at the Back, expected: " << expectedEnd << ", actual: " << *actualEnd << endl;

		if (expectedReturn != actualReturn)
		{
			cout << "FAIL - Bad return" << endl;
		}
		else if (expectedEnd != *actualEnd)
		{
			cout << "FAIL - Bad value" << endl;
		}
		else
		{
			cout << "PASS" << endl;
		}
	}
}

void Tester::Test_Clear()
{
    DrawLine();
    cout << "TEST: Test_Clear" << endl;
	{
		cout << "Test: Push A B C, clear should return 0" << endl;
		List<string> testlist;
		testlist.PushBack("A");
		testlist.PushBack("B");
		testlist.PushBack("C");
		testlist.Clear();

		int expectedSize = 0;
		int actualSize = testlist.Size();


		cout << "Expected size: " << expectedSize << endl;
		cout << "Actual size:   " << actualSize << endl;

		if (actualSize == expectedSize)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
}

void Tester::Test_Get()
{
    DrawLine();
    cout << "TEST: Test_Get" << endl;



	{
		cout << endl << "Test 1: Push Back 'A', 'B', 'C', Get() should return A B C" << endl;

		List<string> testlist;
		testlist.PushBack("A");
		testlist.PushBack("B");
		testlist.PushBack("C");

		string expectedValue = "A";
		string* actualValue = testlist.Get(0);
		string expectedValue2 = "B";
		string* actualValue2 = testlist.Get(1);
		string expectedValue3 = "C";
		string* actualValue3 = testlist.Get(2);

		if (actualValue == nullptr)
		{
			cout << "Nullptr" << endl;
			return;
		}

		cout << "Value, expected: " << expectedValue << ", actual: " << *actualValue << endl;
		cout << "Value, expected: " << expectedValue2 << ", actual: " << *actualValue2 << endl;
		cout << "Value, expected: " << expectedValue3 << ", actual: " << *actualValue3 << endl;

		if (*actualValue != expectedValue ||
			*actualValue2 != expectedValue2 ||
			*actualValue3 != expectedValue3)
		{
			cout << "FAIL - Bad value" << endl;
		}
		else
		{
			cout << "PASS" << endl;
		}
	}
	{
		cout << endl << "Test 2: Get() from empty list should return nullptr." << endl;

		List<string> testlist;

		string* expectedAddress = nullptr;
		string* actualAddress = testlist.Get(0);

		cout << "Address, expected: " << expectedAddress << ", actual: " << actualAddress << endl;

		if (expectedAddress != actualAddress)
		{
			cout << "FAIL - Bad address" << endl;
		}
		else
		{
			cout << "PASS" << endl;
		}
	}
}

void Tester::Test_GetFront()
{
    DrawLine();
    cout << "TEST: Test_GetFront" << endl;

    // Put tests here
    {
        cout << endl << "Test 1: GetFront() from empty list should return nullptr." << endl;

        List<string> testlist;

        string* expectedAddress = nullptr;
        string* actualAddress = testlist.GetFront();

        cout << "Address, expected: " << expectedAddress << ", actual: " << actualAddress << endl;

        if ( expectedAddress != actualAddress )
        {
            cout << "FAIL - Bad address" << endl;
        }
        else
        {
            cout << "PASS" << endl;
        }
    }

    {
        cout << endl << "Test 2: Push Back 'A', 'B', 'C', GetFront() should return A" << endl;

        List<string> testlist;
        testlist.PushBack( "A" );
        testlist.PushBack( "B" );
        testlist.PushBack( "C" );

        string expectedValue = "A";
        string* actualValue = testlist.GetFront();

        if ( actualValue == nullptr )
        {
            cout << "Nullptr" << endl;
            return;
        }

        cout << "Value, expected: " << expectedValue << ", actual: " << *actualValue << endl;

        if ( *actualValue != expectedValue )
        {
            cout << "FAIL - Bad value" << endl;
        }
        else
        {
            cout << "PASS" << endl;
        }
    }
}

void Tester::Test_GetBack()
{
    DrawLine();
    cout << "TEST: Test_GetBack" << endl;
	{
		cout << endl << "Test 1: GetBack() from empty list should return nullptr." << endl;

		List<string> testlist;

		string* expectedAddress = nullptr;
		string* actualAddress = testlist.GetBack();

		cout << "Address, expected: " << expectedAddress << ", actual: " << actualAddress << endl;

		if (expectedAddress != actualAddress)
		{
			cout << "FAIL - Bad address" << endl;
		}
		else
		{
			cout << "PASS" << endl;
		}
	}

	{
		cout << endl << "Test 2: Push Back 'A', 'B', 'C', GetBack() should return C" << endl;

		List<string> testlist;
		testlist.PushBack("A");
		testlist.PushBack("B");
		testlist.PushBack("C");

		string expectedValue = "C";
		string* actualValue = testlist.GetBack();

		if (actualValue == nullptr)
		{
			cout << "Nullptr" << endl;
			return;
		}

		cout << "Value, expected: " << expectedValue << ", actual: " << *actualValue << endl;

		if (*actualValue != expectedValue)
		{
			cout << "FAIL - Bad value" << endl;
		}
		else
		{
			cout << "PASS" << endl;
		}
	}
}

void Tester::Test_GetCountOf()
{
	DrawLine();
	cout << "TEST: Test_GetCountOf" << endl;
	{
		cout << endl << "Test 1: contains nothing GetCountOf() should return 0" << endl;

		List<string> testlist;

		int expectedValue = 0;
		int actualValue = testlist.GetCountOf("A");

		cout << "Value, expected: " << expectedValue << ", actual: " << actualValue << endl;

		if (actualValue != expectedValue)
		{
			cout << "FAIL - Bad value" << endl;
		}
		else
		{
			cout << "PASS" << endl;
		}
	}
	{
		cout << endl << "Test 2: Push Back 'A', 'B', 'C', GetCountOf() should return 1" << endl;

		List<string> testlist;
		testlist.PushBack("A");
		testlist.PushBack("B");
		testlist.PushBack("C");

		int expectedValue = 1;
		int actualValue = testlist.GetCountOf("A");

		if (actualValue == 0)
		{
			cout << "0" << endl;
			return;
		}

		cout << "Value, expected: " << expectedValue << ", actual: " << actualValue << endl;

		if (actualValue != expectedValue)
		{
			cout << "FAIL - Bad value" << endl;
		}
		else
		{
			cout << "PASS" << endl;
		}
	}
	{
		cout << endl << "Test 3: Push Back 'C', 'A', 'K', 'E', 'A', 'A', GetCountOf('A') should return 3 and GetCountOf('Z') should return 0" << endl;

		List<string> testlist;
		testlist.PushBack("C");
		testlist.PushBack("A");
		testlist.PushBack("K");
		testlist.PushBack("E");
		testlist.PushBack("A");
		testlist.PushBack("A");

		int expectedValue = 3;
		int actualValue = testlist.GetCountOf("A");
		int expectedValue2 = 0;
		int actualValue2 = testlist.GetCountOf("Z");


		cout << "Value, expected: " << expectedValue << ", actual: " << actualValue << endl;
		cout << "Value, expected: " << expectedValue2 << ", actual: " << actualValue2 << endl;


		if (actualValue != expectedValue ||
			actualValue2 != expectedValue2)
		{
			cout << "FAIL - Bad value" << endl;
		}
		else
		{
			cout << "PASS" << endl;
		}
	}
	
}

void Tester::Test_Contains()
{
    DrawLine();
    cout << "TEST: Test_Contains" << endl;

	{
		cout << endl << "Test 1: add nothing, Contains('A') should return 0" << endl;

		List<string> testlist;
		
		bool expectedValue = false;
		int actualValue = testlist.Contains("A");

		cout << "Value, expected: " << expectedValue << ", actual: " << actualValue << endl;

		if (actualValue != expectedValue)
		{
			cout << "FAIL - Bad value" << endl;
		}
		else
		{
			cout << "PASS" << endl;
		}
	}

	{
		cout << endl << "Test 2: Push Back 'A', 'B', 'C', Contains('C') should return 1" << endl;

		List<string> testlist;
		testlist.PushBack("A");
		testlist.PushBack("B");
		testlist.PushBack("C");

		bool expectedValue = true;
		int actualValue = testlist.Contains("C");


		cout << "Value, expected: " << expectedValue << ", actual: " << actualValue << endl;

		if (actualValue != expectedValue)
		{
			cout << "FAIL - Bad value" << endl;
		}
		else
		{
			cout << "PASS" << endl;
		}
	}

}

void Tester::Test_Remove()
{
    DrawLine();
    cout << "TEST: Test_Remove" << endl;

	{
		cout << endl << "Test 1: Push Back 'A', 'B', 'C', RemoveItem('B') 0 = A 1 = C and size returns 2" << endl;

		List<string> testlist;
		testlist.PushBack("A");
		testlist.PushBack("B");
		testlist.PushBack("C");
		testlist.RemoveItem("B");
		string expectedValue_0 = "A";
		string expectedValue_1 = "C";

		string* actualValue_0 = testlist.Get(0);
		string* actualValue_1 = testlist.Get(1);

		int expectedSize = 2;
		int actualSize = testlist.Size();


		if (actualValue_0 == nullptr || actualValue_1 == nullptr)
		{
			cout << "nullptrs returned; exiting to avoid segfault" << endl;
			return;
		}

		cout << "Expected value at 0: " << expectedValue_0 << endl;
		cout << "Expected value at 1: " << expectedValue_1 << endl;


		cout << "Actual value at 0: " << *actualValue_0 << endl;
		cout << "Actual value at 1: " << *actualValue_1 << endl;

		cout << "Expected Size = " << expectedSize << endl;
		cout << "Actual Size = " << actualSize << endl;

		if (expectedValue_0 != *actualValue_0 ||
			expectedValue_1 != *actualValue_1 ||
			expectedSize != actualSize)
		{
			cout << "FAIL - Bad value(s)" << endl;
		}
		else
		{
			cout << "PASS" << endl;
		}
	}
	{
		cout << endl << "Test 2: Push Back 'A', 'B', 'C', RemoveItem('Z') Returns False and size returns 2" << endl;

		List<string> testlist;
		testlist.PushBack("A");
		testlist.PushBack("B");
		testlist.PushBack("C");
		testlist.RemoveItem("Z");
		bool expectedValue = false;
		bool actualValue = testlist.RemoveItem("Z");

		int expectedSize = 3;
		int actualSize = testlist.Size();


		cout << "Expected value at 0: " << expectedValue << endl;
	


		cout << "Actual value at 0: " << actualValue << endl;

		cout << "Expected Size = " << expectedSize << endl;
		cout << "Actual Size = " << actualSize << endl;

		if (expectedValue != actualValue ||
			expectedSize != actualSize)
		{
			cout << "FAIL - Bad value(s)" << endl;
		}
		else
		{
			cout << "PASS" << endl;
		}
	}

	{
		cout << endl << "Test 3: Push Back 'C', 'H', 'E', 'E', 'S', 'E', RemoveItem('E') 0 = C 1 = H 2 = S and size returns 3" << endl;

		List<string> testlist;
		testlist.PushBack("C");
		testlist.PushBack("H");
		testlist.PushBack("E");
		testlist.PushBack("E");
		testlist.PushBack("S");
		testlist.PushBack("E");
		testlist.RemoveItem("E");
		string expectedValue_0 = "C";
		string expectedValue_1 = "H";
		string expectedValue_2 = "S";


		string* actualValue_0 = testlist.Get(0);
		string* actualValue_1 = testlist.Get(1);
		string* actualValue_2 = testlist.Get(2);

		int expectedSize = 3;
		int actualSize = testlist.Size();


		if (actualValue_0 == nullptr || actualValue_1 == nullptr || actualValue_2 == nullptr)
		{
			cout << "nullptrs returned; exiting to avoid segfault" << endl;
			return;
		}

		cout << "Expected value at 0: " << expectedValue_0 << endl;
		cout << "Expected value at 1: " << expectedValue_1 << endl;
		cout << "Expected value at 2: " << expectedValue_2 << endl;


		cout << "Actual value at 0: " << *actualValue_0 << endl;
		cout << "Actual value at 1: " << *actualValue_1 << endl;
		cout << "Actual value at 2: " << *actualValue_2 << endl;

		cout << "Expected Size = " << expectedSize << endl;
		cout << "Actual Size = " << actualSize << endl;

		if (expectedValue_0 != *actualValue_0 ||
			expectedValue_1 != *actualValue_1 ||
			expectedValue_2 != *actualValue_2 ||
			expectedSize != actualSize)
		{
			cout << "FAIL - Bad value(s)" << endl;
		}
		else
		{
			cout << "PASS" << endl;
		}
	}
	{
		cout << endl << "Test 1: Push Back 'A', 'B', 'C', RemoveIndex(0) 0 = A 1 = C and size returns 2" << endl;

		List<string> testlist;
		testlist.PushBack("A");
		testlist.PushBack("B");
		testlist.PushBack("C");
		testlist.RemoveIndex(0);
		string expectedValue_0 = "B";
		string expectedValue_1 = "C";

		string* actualValue_0 = testlist.Get(0);
		string* actualValue_1 = testlist.Get(1);

		int expectedSize = 2;
		int actualSize = testlist.Size();


		if (actualValue_0 == nullptr || actualValue_1 == nullptr)
		{
			cout << "nullptrs returned; exiting to avoid segfault" << endl;
			return;
		}

		cout << "Expected value at 0: " << expectedValue_0 << endl;
		cout << "Expected value at 1: " << expectedValue_1 << endl;


		cout << "Actual value at 0: " << *actualValue_0 << endl;
		cout << "Actual value at 1: " << *actualValue_1 << endl;

		cout << "Expected Size = " << expectedSize << endl;
		cout << "Actual Size = " << actualSize << endl;

		if (expectedValue_0 != *actualValue_0 ||
			expectedValue_1 != *actualValue_1 ||
			expectedSize != actualSize)
		{
			cout << "FAIL - Bad value(s)" << endl;
		}
		else
		{
			cout << "PASS" << endl;
		}
	}
	{
		cout << endl << "Test 2: Push Back 'A', 'B', 'C', RemoveIndex(5) returns false and size returns 3" << endl;

		List<string> testlist;
		testlist.PushBack("A");
		testlist.PushBack("B");
		testlist.PushBack("C");
		testlist.RemoveIndex(5);
		bool expectedValue = false;
		bool actualValue = testlist.RemoveIndex(5);

		int expectedSize = 3;
		int actualSize = testlist.Size();


		cout << "Expected value at 5: " << expectedValue << endl;



		cout << "Actual value at 5: " << actualValue << endl;

		cout << "Expected Size = " << expectedSize << endl;
		cout << "Actual Size = " << actualSize << endl;

		if (expectedValue != actualValue ||
			expectedSize != actualSize)
		{
			cout << "FAIL - Bad value(s)" << endl;
		}
		else
		{
			cout << "PASS" << endl;
		}
	}

	{
		cout << endl << "Test 3: Push Back 'A', 'B', 'C', RemoveIndex(2) 0 = A 1 = C and size returns 5" << endl;

		List<string> testlist;
		testlist.PushBack("C");
		testlist.PushBack("H");
		testlist.PushBack("E");
		testlist.PushBack("E");
		testlist.PushBack("S");
		testlist.PushBack("E");
		testlist.RemoveIndex(2);
		string expectedValue_0 = "C";
		string expectedValue_1 = "H";
		string expectedValue_2 = "E";
		string expectedValue_3 = "S";
		string expectedValue_4 = "E";



		string* actualValue_0 = testlist.Get(0);
		string* actualValue_1 = testlist.Get(1);
		string* actualValue_2 = testlist.Get(2);
		string* actualValue_3 = testlist.Get(3);
		string* actualValue_4 = testlist.Get(4);


		int expectedSize = 5;
		int actualSize = testlist.Size();

		cout << "Expected value at 0: " << expectedValue_0 << endl;
		cout << "Expected value at 1: " << expectedValue_1 << endl;
		cout << "Expected value at 2: " << expectedValue_2 << endl;
		cout << "Expected value at 3: " << expectedValue_3 << endl;
		cout << "Expected value at 4: " << expectedValue_4 << endl;


		cout << "Actual value at 0: " << *actualValue_0 << endl;
		cout << "Actual value at 1: " << *actualValue_1 << endl;
		cout << "Actual value at 2: " << *actualValue_2 << endl;
		cout << "Actual value at 3: " << *actualValue_3 << endl;
		cout << "Actual value at 4: " << *actualValue_4 << endl;

		cout << "Expected Size = " << expectedSize << endl;
		cout << "Actual Size = " << actualSize << endl;

		if (expectedValue_0 != *actualValue_0 ||
			expectedValue_1 != *actualValue_1 ||
			expectedValue_2 != *actualValue_2 ||
			expectedValue_3 != *actualValue_3 ||
			expectedValue_4 != *actualValue_4 ||
			expectedSize != actualSize)
		{
			cout << "FAIL - Bad value(s)" << endl;
		}
		else
		{
			cout << "PASS" << endl;
		}
	}
}

void Tester::Test_Insert()
{
    DrawLine();
    cout << "TEST: Test_Insert" << endl;
	{
		cout << endl << "Test 1: Insert(0, 'A') should return Get(0) = A" << endl;

		List<string> testlist;
		testlist.Insert(0, "A");
		string expectedValue = "A";
		string* actualValue = testlist.Get(0);

		int expectedSize = 1;
		int actualSize = testlist.Size();

		if (actualValue == nullptr)
		{
			cout << "Nullptr" << endl;
			return;
		}

		cout << "Value, expected: " << expectedValue << ", actual: " << *actualValue << endl;

		if (*actualValue != expectedValue)
		{
			cout << "FAIL - Bad value" << endl;
		}
		else
		{
			cout << "PASS" << endl;
		}
	}

	{
		cout << endl << "Test 2: Insert(50, 'A') should return Get(50) = false or 0 not Contiguous" << endl;

		List<string> testlist;

		testlist.Insert(50,"A");
		bool expectedValue = false;
		bool actualValue = testlist.Get(50);

		int expectedSize = 0;
		int actualSize = testlist.Size();


		cout << "Value, expected: " << expectedValue << ", actual: " << actualValue << endl;

		if (actualValue != expectedValue)
		{
			cout << "FAIL - Bad value" << endl;
		}
		else
		{
			cout << "PASS" << endl;
		}
	}
	{
		cout << endl << "Test 3: Insert 'A', 'B', 'C', should return A B C and size 3" << endl;

		List<string> testlist;

		testlist.Insert(0, "A");
		testlist.Insert(1, "B");
		testlist.Insert(2, "C");
		string expectedValue = "A";
		string* actualValue = testlist.Get(0);
		string expectedValue2 = "B";
		string* actualValue2 = testlist.Get(1);
		string expectedValue3 = "C";
		string* actualValue3 = testlist.Get(2);

		int expectedSize = 3;
		int actualSize = testlist.Size();

		if (actualValue == nullptr)
		{
			cout << "Nullptr" << endl;
			return;
		}

		cout << "Value, expected: " << expectedValue << ", actual: " << *actualValue << endl;
		cout << "Value, expected: " << expectedValue2 << ", actual: " << *actualValue2 << endl;
		cout << "Value, expected: " << expectedValue3 << ", actual: " << *actualValue3 << endl;
		cout << "Value, expected: " << expectedSize << ", actual: " << actualSize << endl;


		if (*actualValue != expectedValue ||
			*actualValue2 != expectedValue2 ||
			*actualValue3 != expectedValue3 ||
			actualSize != expectedSize)
		{
			cout << "FAIL - Bad value" << endl;
		}
		else
		{
			cout << "PASS" << endl;
		}
	}
}
