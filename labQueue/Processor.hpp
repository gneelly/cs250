#ifndef _PROCESSOR
#define _PROCESSOR

#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
using namespace std;

#include "Job.hpp"
#include "Queue.hpp"

class Processor
{
    public:
    void FirstComeFirstServe( vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile );
    void RoundRobin( vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile );
};

void Processor::FirstComeFirstServe( vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile )
{
	ofstream output(logFile);
	output << "First Come, First Served" << endl;
	int cycles = 0;
	output << "Job: " << jobQueue.Front()->id << endl;
	Queue<Job*> sumJobs;
	while (jobQueue.Size() > 0)
	{

		output << "CYCLE: " << cycles << "\t";
		//process the front most item
		jobQueue.Front()->Work(FCFS);
		output << "REMAINING: " << jobQueue.Front()->fcfs_timeRemaining << "\n";
		if (jobQueue.Front()->fcfs_done)
		{
			jobQueue.Front()->SetFinishTime(cycles, FCFS);
			//Set the front most item's finish time
			output << "Total time: " << jobQueue.Front()->fcfs_finishTime << endl;
			sumJobs.Push(jobQueue.Front());
			//pop the item off the jobQueue
			jobQueue.Pop();
			if (jobQueue.Size() > 0) {
				output << "Job: " << jobQueue.Front()->id + 1 << endl;
			}
		}
		cycles++;
	}
	output << endl << endl << "Summary of jobs: " << endl << endl;
	while (sumJobs.Size() > 1) {
		output << "Job: " << sumJobs.Front()->id << "\t" << "Time: " << sumJobs.Front()->fcfs_finishTime << endl;
		sumJobs.Pop();
	}
	output << "Job: " << sumJobs.Front()->id << "\t" << "Time: " << sumJobs.Front()->fcfs_finishTime << endl;
	output << "Total time: " << sumJobs.Front()->fcfs_finishTime << endl;
	output.close();
}

void Processor::RoundRobin( vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile )
{
	
	ofstream output(logFile);
	output << "Round Robin" << endl;
	output << "Job: " << jobQueue.Front()->id << endl;
	int cycles = 0;
	int timer = 0;
	Queue<Job*> sumJobs;
	while (jobQueue.Size() > 0)
	{
		if (timer == timePerProcess)
		{
			jobQueue.Front()->rr_timesInterrupted++;
			jobQueue.Push(jobQueue.Front());
			jobQueue.Pop();
			timer = 0;

		}
		output << "CYCLE: " << cycles << "\t";
		//process the front most item
		jobQueue.Front()->Work(RR);
		output << "REMAINING: " << jobQueue.Front()->rr_timeRemaining << "\n";
		if (jobQueue.Front()->rr_done)
		{
			jobQueue.Front()->SetFinishTime(cycles, RR);
			//Set the front most item's finish time
			output << "Total time: " << jobQueue.Front()->rr_finishTime << endl;
			output << "Times interrupted: " << jobQueue.Front()->rr_timesInterrupted << endl;
			sumJobs.Push(jobQueue.Front());
			//pop the item off the jobQueue
			jobQueue.Pop();
			if (jobQueue.Size() > 0) {
				output << "Job: " << jobQueue.Front()->id + 1 << endl;

			}
		}
		cycles++;
		timer++;
	}
	output << endl << endl << "Summary of jobs: " << endl << endl;
	while (sumJobs.Size() > 1) {
		output << "Job: " << sumJobs.Front()->id << "\t" << "Time: " << sumJobs.Front()->rr_finishTime << " Times interrupted: " << sumJobs.Front()->rr_timesInterrupted << endl;
		sumJobs.Pop();
	}
	output << "Job: " << sumJobs.Front()->id << "\t" << "Time: " << sumJobs.Front()->rr_finishTime <<" Times interrupted: "<< sumJobs.Front()->rr_timesInterrupted << endl;
	output << "Total time: " << sumJobs.Front()->rr_finishTime << endl;

	output.close();
	
}

#endif
