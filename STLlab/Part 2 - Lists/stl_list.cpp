// Lab - Standard Template Library - Part 2 - Lists
// Garrin, Neelly

#include <iostream>
#include <list>
#include <string>
using namespace std;

void DisplayList(list<string>& states) {
	for (list<string>::iterator it = states.begin();
		it != states.end();
		it++) {
		cout << *it << "\t";
	}
	cout << endl;
}

int main()
{
	list<string> states;

	bool done = false;
	while (!done)
	{
		cout << "1. Push front "
		<< "2. Push back "
		<< "3. Pop front "
		<< "4. Pop back "
		<< "5. Continue " << endl;

		int choice;
		cin >> choice;
		
		switch (choice) {
		case 1:
		{
			cout << "Enter a new state: ";
			string state;
			cin >> state;
			states.push_front(state);
			break;
		}
		case 2:
		{
			cout << "Enter a new state: ";
			string state;
			cin >> state;
			states.push_back(state);
			break;
		}
		case 3:
		{
			states.pop_front();
			break;
		}
		case 4:
		{
			states.pop_back();
			break;
		}
		case 5:
			
			done = true;
			break;
		}
	}
	cout << "Original: " << endl;
	DisplayList(states);
	cout << "Reversed: " << endl;
	states.reverse();
	DisplayList(states);
	cout << "Sorted: " << endl;
	states.reverse();
	states.sort();
	DisplayList(states);
	cout << "Sorted-reversed: " << endl;
	states.reverse();
	DisplayList(states);

    cin.ignore();
    cin.get();
    return 0;
}
