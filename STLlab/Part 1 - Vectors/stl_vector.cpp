// Lab - Standard Template Library - Part 1 - Vectors
// Garrin, Neelly

#include <iostream>
#include <vector>
#include <string>
using namespace std;

int main()
{
	vector<string> courses; //string[] courses

	bool done = false;
	while (!done)
	{
		cout << endl << endl << "Main Menu: " << endl
			<< "1. Add a new course, "
			<< "2. Remove the last course, "
			<< "3. Display the course list, "
			<< "4. Quit" << endl;

		int choice;
		cin >> choice;

		if (choice == 1) {//add course
			string courseName;//temp var
			cout << "Please enter the course: ";
			//cin >> courseName;
			cin.ignore();//for buffer 
			getline(cin, courseName);//allows spaces
			courses.push_back(courseName);//add to list of courses
		}
		if (choice == 2) {//remove course
			courses.pop_back();//remove last item in list of courses
		}
		if (choice == 3) {//display list
			for (unsigned int i = 0; i < courses.size(); i++) {
				cout << i << ". " << courses[i] << endl;
			}
		}
		if (choice == 4) {//quit
			done = true;
		}
	}
    cin.ignore();
    cin.get();
    return 0;
}
